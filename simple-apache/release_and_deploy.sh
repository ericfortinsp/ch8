#!/bin/sh
set -e -x

bosh -n target $target apache2
bosh -n login ${username} ${password}
cd resource-bosh-release/simple-apache/
bosh upload release dev_releases/simple-apache/simple-apache-0+dev.4.yml

bosh deployment resource-bosh-release/simple-apache/simple-apache.yml
bosh -n deploy
